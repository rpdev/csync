/*
 * libcsync -- a library to sync a directory with another
 *
 * Copyright (c) 2016 by Martin Hoeher <martin@rpdev.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "csync_modules.h"

const csync_module_interface_t csync_module_list[] = {
    #ifdef HAS_DUMMY_MODULE
    {"dummy", csync_dummy_vio_module_init, csync_dummy_vio_module_shutdown},
    #endif
    
    #ifdef HAS_OWNCLOUD_MODULE
    {"owncloud", csync_owncloud_vio_module_init, csync_owncloud_vio_module_shutdown},
    #endif
    
    #ifdef HAS_SFTP_MODULE
    {"sftp", csync_sftp_vio_module_init, csync_sftp_vio_module_shutdown},
    #endif
    
    #ifdef HAS_SMB_MODULE
    {"smb", csync_smb_vio_module_init, csync_smb_vio_module_shutdown},
    #endif
    
    {NULL, NULL, NULL}

};
