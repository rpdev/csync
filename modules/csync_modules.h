/*
 * libcsync -- a library to sync a directory with another
 *
 * Copyright (c) 2016 by Martin Hoeher <martin@rpdev.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */


#include "config.h"
#include "vio/csync_vio_module.h"
#include "vio/csync_vio_method.h"


#ifdef WITH_BUILTIN_MODULES

typedef struct csync_module_interface {
    const char                  *proto;
    csync_vio_method_init_fn       init;
    csync_vio_method_finish_fn   shutdown;
} csync_module_interface_t;

extern const csync_module_interface_t csync_module_list[];

// Dummy module interface
csync_vio_method_t *csync_dummy_vio_module_init(const char *method_name, const char *args,
    csync_auth_callback cb, void *userdata);
void csync_dummy_vio_module_shutdown(csync_vio_method_t *method);

// Samba module interface
csync_vio_method_t * csync_smb_vio_module_init(const char *method_name, const char *args,
    csync_auth_callback cb, void *userdata);
void csync_smb_vio_module_shutdown(csync_vio_method_t *method);

// SFTP module interface
csync_vio_method_t * csync_sftp_vio_module_init(const char *method_name, const char *args,
    csync_auth_callback cb, void *userdata);
void csync_sftp_vio_module_shutdown(csync_vio_method_t *method);

// OwnCloud module interface
csync_vio_method_t * csync_owncloud_vio_module_init(const char *method_name, const char *args,
                                    csync_auth_callback cb, void *userdata);
void csync_owncloud_vio_module_shutdown(csync_vio_method_t *method);

#endif
